import React, { Component } from 'react';
import Trades from './components/Trades';
import Transactions from './components/Transactions';
import Profit from './components/Profit';
import BuyOrSell from './components/BuyOrSell';
import Deposit from './components/Deposit'
import Balance from './components/Balance';
import Diversity from './components/Diversity';
import News from './components/News';
class App extends Component {

  state = {

    trades: [],
    transactions: [],
    profit: [],
    balance: []
  }


  componentDidMount() {
    Promise.all([  
    fetch('http://localhost:8080/trades/profit'),
    fetch('http://localhost:8080/trades/balance')])

      .then(([res1, res2]) => {
        return Promise.all([res1.json(), res2.json()])
      })
      .then(([res1, res2]) => {
        // set state in here
        this.setState({ profit: res1, balance: res2})
      });
  }


  render() {

    return (
      <div id="App">
        <br></br>
        <center><BuyOrSell/></center>
        <br></br>
        <div class="row justify-content-center">
          <Profit profit={this.state.profit} />
          <Deposit/>
          <Balance balance={this.state.balance}/>
        </div>
        <br></br>
        <News/>
        <br></br>
         <Trades/> 
         <Diversity/>
        <br></br>
        <Transactions/>
        <br></br>

      </div>
    );

  }
}
export default App;
