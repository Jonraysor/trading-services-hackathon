import React, { Component, TextInput } from 'react'

export default class Deposit extends Component {
    constructor(props) {
        super(props)
        this.state = {
            DepositFunds: []
        };

        this.handleFunds = this.handleFunds.bind(this);
        this.handleMoney = this.handleMoney.bind(this);
    }

    handleFunds(event) {
        const money = event.target.value;
        this.setState({ DepositFunds: money });
    }

    handleMoney(event) {
        alert(`Are you sure you want to deposit ${this.state.DepositFunds}?`)
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ ticker: "USD", price: 1, quantity: this.state.DepositFunds, state: "DEPOSITED" })
        };

        fetch('http://localhost:8080/trades', requestOptions)
            .then(response => response.json())
            .then(data => this.setState({ DepositFunds: data }));
    }

    render() {
        return (
            <div class="col-xl-3 col-md-6 mb-4 align-items-center justify-content-center">
                <div class="card border-left-primary shadow h-100 py-2 bg-secondary">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <center><h5 class="card-title">Deposit Funds</h5></center>
                                <br></br>
                                <form class="form-inline d-flex justify-content-center" id="deposit">
                                    <input type="number" class="form-control" placeholder="Deposit"
                                        value={this.state.DepositFunds} onChange={this.handleFunds}
                                    />
                                    <button type="submit" class="btn btn-success" style={{ margin: '0.8em' }} id="deposit" onClick={this.handleMoney}>Deposit</button>
                                </form>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-calendar fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
