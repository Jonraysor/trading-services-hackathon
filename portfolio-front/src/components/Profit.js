import React from 'react'
import BuyOrSell from './BuyOrSell';

const Profit = ({ profit }) => {
    return (

        <div class="align-item-center col-xl-3 col-md-6 mb-4 ">

                <div class="card border-left-primary shadow h-100 py-2 bg-secondary" id="balance">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <br></br>
                            <br></br>
                            <center><h4 class="text-xs font-weight-bold text-primary text-uppercase mb-1">Profits</h4></center>
                            <center><div class="h5 mb-0 font-weight-bold text-gray-800">{profit}</div></center>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}






export default Profit