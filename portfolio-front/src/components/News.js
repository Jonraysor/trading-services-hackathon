
import React, { Component, TextInput } from 'react'
import moment from 'moment';

export default class News extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            articles: []
        };
    }

    componentDidMount() {
        fetch('https://newsapi.org/v2/top-headlines?country=' +
            'us&apiKey=708d794ed840406490b86ea651926ead')
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        articles: result.articles
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }
    render() {
        const { error, isLoaded, articles } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div id="news" class="container justify-content-center">
                    <div class="card">
                        {
                            articles.map(article => {
                                return (
                                    <div class="list-group">
                                        <a href="#" class="list-group-item list-group-item-action">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">{article.title}</h5>
                                                <small>{moment(article.publishedAt).format('MM/DD/YYYY h:mm a')}</small>
                                            </div>
                                            <p class="mb-1">{article.content}</p>
                                            <a href={article.url}>
                                                <small>{article.url}</small>
                                            </a>
                                        </a> 
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            );

        }
    }
}