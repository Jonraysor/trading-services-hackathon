import React, { useState, useEffect } from "react";
import { Doughnut } from "react-chartjs-2";
import axios from "axios";

const Dankmemes = () => {
  const [chartData, setChartData] = useState({});
  const [employeeSalary, setEmployeeSalary] = useState([]);
  const [employeeAge, setEmployeeAge] = useState([]);

  const chart = () => {
    let empSal = [];
    let empAge = [];
    let colors = [];
    axios
      .get("http://localhost:8080/trades/portfolio")
      .then(res => {
        //console.log(res.data[2]);
        for (const dataObj of res.data) {
          if (dataObj.ticker == "USD"){continue;}
          empSal.push(parseInt(dataObj.percPortfolio));
          empAge.push(dataObj.ticker);
          let r = Math.floor(Math.random() * 200);
          let g = Math.floor(Math.random() * 200);
          let b = Math.floor(Math.random() * 200);
          let color = 'rgb(' + r + ', ' + g + ', ' + b + ')';
    colors.push(color);


          
        }
        //empSal.push(parseInt(res.data[2].percPortfolio));
        //  empAge.push((res.data[2].ticker));
        setChartData({
          labels: empAge,
          datasets: [
            {
              label: "level of thiccness",
              data: empSal,
              backgroundColor: colors,
              borderWidth: 4
            }
          ]
        });
      })
      .catch(err => {
        console.log(err);
      });
    console.log(empSal, empAge);
  };

  useEffect(() => {
    chart();
  }, []);
  return (
    <div className="App">
      <h1>Dankmemes</h1>
      <div>
        <Doughnut
          data={chartData}
          options={{
            responsive: true,
            title: { text: "THICCNESS SCALE", display: true },
            
          }}
        />
      </div>
    </div>
  );
};

export default Dankmemes;  