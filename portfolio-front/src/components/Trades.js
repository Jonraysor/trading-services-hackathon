import React, { Component, TextInput } from 'react'

export default class Trades2 extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        error: null,
        isLoaded: false,
        trades: []
      };
    }
  
    componentDidMount() {
        fetch('http://localhost:8080/trades/portfolio')
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              isLoaded: true,
              trades: result
            });
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
    }
  
    render() {
      const { error, isLoaded, trades } = this.state;
      if (error) {
        return <div>Error: {error.message}</div>;
      } else if (!isLoaded) {
        return <center><div>Loading...</div></center>;
      } else {
        return (
        
          <div class="justify-content-center customTable">
            <center><h1>Portfolio</h1></center>
            <table class="table table-striped table-dark">
                <thead>
                    <tr>
                        <th scope="col">Ticker</th>
                        <th scope="col">Price</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Profit</th>
                    </tr>
                </thead>
                {trades.map((trade) => (
                    <tbody>
                        <tr>
                            
                            <td>{trade.ticker}</td>
                            <td>{trade.price}</td>
                            <td>{trade.quantity}</td>
                            <td>{trade.profit}</td>
                        </tr>
                    </tbody>
                )
                )
                }
            </table>
        </div>
        
        );
      }
    }
  }
