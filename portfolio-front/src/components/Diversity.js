import React, { useState, useEffect } from "react";
import { Doughnut } from "react-chartjs-2";
import axios from "axios";

const Diversity = () => {
  const [chartData, setChartData] = useState({});
  const [employeeSalary, setEmployeeSalary] = useState([]);
  const [employeeAge, setEmployeeAge] = useState([]);

  const chart = () => {
    let empSal = [];
    let empAge = [];
    let colors = [];
    axios
      .get("http://localhost:8080/trades/portfolio")
      .then(res => {
  
        for (const dataObj of res.data) {
          if (dataObj.ticker == "USD"){continue;}
          empSal.push(parseInt(dataObj.percPortfolio));
          empAge.push(dataObj.ticker);
          let r = Math.floor(Math.random() * 200);
          let g = Math.floor(Math.random() * 200);
          let b = Math.floor(Math.random() * 200);
          let color = 'rgb(' + r + ', ' + g + ', ' + b + ')';
    colors.push(color);
        }

        setChartData({
          labels: empAge,
          datasets: [
            {
              data: empSal,
              backgroundColor: colors,
              borderWidth: 4
            }
          ]
        });
      })
      .catch(err => {
        console.log(err);
      });
    console.log(empSal, empAge);
  };

  useEffect(() => {
    chart();
  }, []);
  return (
    <div className="App" id="ourChart">
      <center><h1>Diversity</h1></center>
      <div>
        <Doughnut
          data={chartData}
          width={500}
          height={500}
          options={{
            maintainAspectRatio: false,
            responsive: true
          }}
        />
      </div>
    </div>
  );
};

export default Diversity;  