import React, { Component, TextInput } from 'react'

export default class Transactions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      transactions: []
    };
  }

  componentDidMount() {
    fetch('http://localhost:8080/trades/transactions')
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            transactions: result
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, transactions } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div class="justify-content-center customTable">
          <center><h1>Transactions</h1></center>
          <table class="table table-striped table-dark">
            <thead>
              <tr>
                <th scope="col">Date</th>
                <th scope="col">Ticker</th>
                <th scope="col">Price</th>
                <th scope="col">Quantity</th>``
              </tr>
            </thead>
            {transactions.map((trade) => (
              <tbody>
                <tr>
                  <td>{trade.date}</td>
                  <td>{trade.ticker}</td>
                  <td>{trade.price}</td>
                  <td>{trade.quantity}</td>
                </tr>
              </tbody>
            )
            )
            }
          </table>
        </div>
      );
    }
  }
}

