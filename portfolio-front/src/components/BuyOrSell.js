import React, { Component, TextInput } from 'react'

export default class BuyOrSell extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            tickerName: 'ticker',
            quantity: 'quantity'
        };

        this.handleChangeTicker = this.handleChangeTicker.bind(this);
        this.handleChangeQuantity = this.handleChangeQuantity.bind(this);
        this.handleBuy = this.handleBuy.bind(this);
        this.handleSell = this.handleSell.bind(this);

    }

    handleChangeTicker(event) {
        const ticker = event.target.value;
        this.setState({ tickerName: ticker });
    }

    handleChangeQuantity(event) {
        const quantity = event.target.value;
        this.setState({ quantity: quantity });
    }

    handleBuy(event) {
        alert(`Submitting BUY for ${this.state.tickerName} for ${this.state.quantity} share(s).`)
        // Simple POST request with a JSON body using fetch
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ ticker: this.state.tickerName, quantity: this.state.quantity, state: "CREATED" })
        };

        fetch('http://localhost:8080/trades', requestOptions)
            .then(response => response.json())
            .then(data => this.setState({ postId: data.id }));
    }

    handleSell(event) {

        alert(`Submitting SELL for ${this.state.tickerName} for ${this.state.quantity} share(s).`)
        // Simple POST request with a JSON body using fetch
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ ticker: this.state.tickerName, quantity: this.state.quantity })
        };

        fetch('http://localhost:8080/trades/sell', requestOptions)
            .then(response => response.json())
            //.then(data => this.setState({ postId: data.id }));
            .then(
                (result) => {
                  this.setState({
                    isLoaded: true,
                    postId: result
                  });
                },
                // Error check!
                (error) => {
                  this.setState({
                    isLoaded: true,
                    error
                  });
                }
              )

        window.location.reload();
    }

    render() {
        const { error, isLoaded } = this.state;
        if (error) {
            return alert(`I'm sorry, we can't make the trade you're looking for`);
        } else {
        return (
            <div class="col-xl-3 col-md-6 mb-4 align-items-center justify-content-center bg-secondary">
                <h5 class="card-header bg-secondary">Make a Trade</h5>
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div class="col">
                                    <input type="text" class="form-control" placeholder="Ticker"
                                        value={this.state.ticker} onChange={this.handleChangeTicker}
                                    />
                                </div>
                                <div class="col">
                                    <input type="number" class="form-control" placeholder="Quantity"
                                        value={this.state.quantity} onChange={this.handleChangeQuantity} />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <button className="Button" class="btn btn-primary mb-2" id="buyButton" style={{ margin: '0.8em' }} onClick={this.handleBuy}>Buy</button>
                                </div>
                                <div class="col">
                                    <button className="Button" class="btn btn-primary mb-2" id="sellButton" style={{ margin: '0.8em' }} onClick={this.handleSell}>Sell</button>
                                </div>
                            </div>   
                        </form>
                    </div>
                </div>

        )
        }
    }
}


