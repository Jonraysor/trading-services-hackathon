POST http://localhost:8080/trades HTTP/1.1
Content-Type: application/json

{
    "ticker" : "USD",
    "price" : 1,
    "quantity" : 100000,
    "state" : "DEPOSITED"
}