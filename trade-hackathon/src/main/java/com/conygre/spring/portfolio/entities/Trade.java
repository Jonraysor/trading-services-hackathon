package com.conygre.spring.portfolio.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date; 

@Document
public class Trade {

    @Id
    private ObjectId id;
    private String ticker;
    private double price;
    private double quantity;
    private String state;
    private double value;
    private String profit;
    private double percPortfolio;
    private String date;

    public Trade(String ticker, double price, double quantity, String state) {
        this.ticker = ticker;
        this.price = price;
        this.quantity = quantity;
        this.state = state;
        this.value = price * quantity;
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        this.date = dateFormat.format(date);
    }
    
    public void setProfit(String profit){
        this.profit = profit;
    }

    public String getProfit(){
        return this.profit;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Trade() {

    }



    @Override
    public String toString() {
    
        return ticker + " " + price + " " +quantity + " value " + value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getPercPortfolio() {
        return percPortfolio;
    }

    public void setPercPortfolio(double percPortfolio) {
        this.percPortfolio = percPortfolio;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


}