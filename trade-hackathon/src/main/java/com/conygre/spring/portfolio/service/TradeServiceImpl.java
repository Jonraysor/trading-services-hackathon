package com.conygre.spring.portfolio.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;


import com.conygre.spring.portfolio.entities.Trade;
import com.conygre.spring.portfolio.repo.TradeRepository;
//import com.fasterxml.jackson.core.JsonParser;
import java.io.BufferedReader;

import java.io.Reader;
import java.nio.charset.Charset;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private TradeRepository repo;

    private static DecimalFormat df = new DecimalFormat("0.00");

    @Override
    public Collection<Trade> getTrades() {
        return repo.findAll(Sort.by(Sort.Direction.DESC, "date"));
    }

    @Override
    public void addTrade(Trade Trade) throws JSONException {

        if (Trade.getTicker().equals("USD")) {
            repo.insert(Trade);

        } else {

            double currentPrice = getJSON(Trade.getTicker());
            currentPrice =  Math.floor(currentPrice * 100) / 100;
            Trade.setPrice(currentPrice);
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            Trade.setDate(dateFormat.format(date));

            HashMap<String, Trade> currentPositions = getCurrentPositions();
            Trade cashAvailable = currentPositions.get("USD");
            double tradeCost = currentPrice * Trade.getQuantity();
            tradeCost = Math.floor(tradeCost * 100) / 100;
            if (tradeCost <= cashAvailable.getValue()) {

                Trade cashTransaction = new Trade("USD", 1.0, -1 * tradeCost, "WITHDRAWN");

                repo.insert(cashTransaction);
                Trade.setValue(tradeCost);
                repo.insert(Trade);

            } else {
                System.out.println("Not enough cash");
            }
        }

    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    public static double getJSON(String ticker) throws JSONException {
        JSONObject json;
        try {
            json = readJsonFromUrl(
                    "https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed?ticker=" + ticker);

            JSONObject obj = new JSONObject(json.toString());

            JSONArray arr = obj.getJSONArray("price_data");
            JSONArray inner_array = arr.getJSONArray(0);
            // System.out.println("\n\n " + inner_array);
            double price = inner_array.getDouble(1);
            // System.out.println("\n\n" + price);
            return price;

        } catch (IOException e) {

            e.printStackTrace();
        }

        return -1;
    }

    @Override
    public ArrayList<Trade> getPctHoldings(){
        ArrayList<Trade> portfolio = currentPosList();

        double valueSum = 0;
       for (Trade pos : portfolio){
            if (pos.getTicker().equals("USD")){
                continue;
            }
            valueSum += pos.getValue();
       }

        for (Trade find : portfolio){
            double val = find.getValue();
            double percent = val / valueSum;
            percent = Math.floor(percent * 100);
            find.setPercPortfolio(percent);
        }
        return portfolio;
    }

    @Override
    public Trade findByTicker(String ticker) {
        return repo.findByTicker(ticker);
    }

    @Override
    public Optional<Trade> findById(ObjectId id) {
        return repo.findById(id);
    }

    @Override
    public void updateState(ObjectId id, String state) {
        Trade Trade = repo.findById(id).get();
        Trade.setState(state);
    }

    @Override
    public void deleteById(ObjectId id) {
        repo.deleteById(id);
    }

    @Override
    public HashMap<String, Trade> getCurrentPositions() {

        Collection<Trade> Trades = repo.findAll();
        HashMap<String, Trade> portfolio = new HashMap<String, Trade>();

        for (Trade Trade : Trades) {

            double current_transaction_value = Trade.getQuantity() * Trade.getPrice();
            current_transaction_value =  Math.floor(current_transaction_value * 100) / 100;
            // System.out.println(Trade.getTicker());
            if (!portfolio.containsKey(Trade.getTicker())) {
                // System.out.println("new ticker \n\n");
                
                Trade.setValue(current_transaction_value);
                portfolio.put(Trade.getTicker(), Trade);
            }

            else if (portfolio.containsKey(Trade.getTicker())) {
                // System.out.println("update ticker \n\n");
                Trade portfolioTrade = portfolio.get(Trade.getTicker());
                double current_quantity = portfolioTrade.getQuantity();
                double new_quantity = current_quantity + Trade.getQuantity();
                portfolioTrade.setQuantity(new_quantity);

                double new_Trade_value = portfolioTrade.getValue() + current_transaction_value;
                portfolioTrade.setValue(new_Trade_value);

                portfolio.put(Trade.getTicker(), portfolioTrade);
            }
        }
        // portfolio.remove("USD");

        return portfolio;
    }

    @Override
    public ArrayList<Trade> currentPosList() {

        ArrayList<Trade> port_trades = new ArrayList<Trade>();

        HashMap<String, Trade> currentPos = getCurrentPositions();

        currentPos.forEach((ticker, values) -> port_trades.add(values));

        ArrayList<Trade> portfolio = getEarnings(port_trades);
        // portfolio.remove()
        return portfolio;
    }

    @Override
    public ArrayList<Trade> getEarnings(ArrayList<Trade> portfolio) {

        for (Trade trade : portfolio) {

            String tradeTicker = trade.getTicker();
            double current = getJSON(tradeTicker);

            double currentTickerVal = current * trade.getQuantity();
            double cost = trade.getValue();

            double profit = currentTickerVal - cost;
            String p = df.format(profit);

            trade.setProfit(p);
        }
        return portfolio;
    }

    @Override
    public String getBalance() {

        Double balance = 0.0;

        ArrayList<Trade> port_trades = new ArrayList<Trade>();

        port_trades = currentPosList();

        for (Trade trade : port_trades) {
            System.out.println(trade);
            if (trade.getTicker().equals("USD")) {
                balance = trade.getQuantity();
            }
        }
        String fBalance = df.format(balance);
        return fBalance;
    }

    @Override
    public String getPortfolioProfit() {
        ArrayList<Trade> positions = currentPosList();
        ArrayList<Trade> trades = getEarnings(positions);

        double total = 0;
        for (Trade trade : trades) {

            if (trade.getTicker().equals("USD")) {
                continue;
            }
            String p = trade.getProfit();
            double profit = Double.valueOf(p);
            total += profit;
        }
        String totalStr = df.format(total);
        return totalStr;
    }

    @Override
    public void sellStock(String ticker, double quantity) {

        HashMap<String, Trade> currentPositions = getCurrentPositions();
        System.out.println(currentPositions.get(ticker));
        Trade stockToSellPos = currentPositions.get(ticker);
        if (quantity <= stockToSellPos.getQuantity()) {
            double currentPrice = getJSON(ticker);
            Trade trade = new Trade(ticker, currentPrice, -1 * quantity, "FILLED");

            double value = quantity * currentPrice;

            repo.insert(trade);

            Trade addToUSD = new Trade("USD", 1.0, value, "DEPOSITED");

            repo.insert(addToUSD);
        }

        else {
            System.out.println("Not enough shares to sell");
        }
    }
}