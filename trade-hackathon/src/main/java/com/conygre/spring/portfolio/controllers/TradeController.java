package com.conygre.spring.portfolio.controllers;

import java.util.ArrayList;
import java.util.Collection;


import com.conygre.spring.portfolio.entities.Trade;
import com.conygre.spring.portfolio.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 30)
@RestController
@RequestMapping("trades")
public class TradeController {

    @Autowired
    private TradeService service;

    @RequestMapping(method = RequestMethod.POST)
    public void addTrade(@RequestBody Trade Trade) {
        service.addTrade(Trade);
    }

    @RequestMapping(method = RequestMethod.POST, value="/sell")
    public void sellStock(@RequestBody Trade Trade){
        service.sellStock(Trade.getTicker(), Trade.getQuantity());
    }

    @RequestMapping(method = RequestMethod.GET, value="/{ticker}")
    public Trade findByTicker(@PathVariable("ticker") String ticker){
        return service.findByTicker(ticker);
        
    }

    @RequestMapping(method = RequestMethod.GET, value = "/portfolio")
    public ArrayList<Trade> getPctHoldings(){
        return service.getPctHoldings();
        
    }

    @RequestMapping(method = RequestMethod.GET, value = "/profit")
    public String getTotalProfit(){
        return service.getPortfolioProfit();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/transactions")
    public  Collection<Trade> getTrades() {
        return service.getTrades();  
    }

    @RequestMapping(method = RequestMethod.GET, value="/trade/{id}")
    public Trade findById(@PathVariable("id") String id){
        return service.findById(new ObjectId(id)).get();
        
    }

    @RequestMapping(method = RequestMethod.DELETE, value="/delete/{id}")
    public void deleteById(@PathVariable("id") String id){
        service.deleteById(new ObjectId(id));
        
    } 

    @RequestMapping(value = "/id/{id}/status/{status}", method = RequestMethod.PUT)
    public void  updateTrade(@PathVariable("id") String id, @PathVariable("status") String status){
        service.updateState(new ObjectId(id), status);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/balance")
    public String getBalance() {
        return service.getBalance();  
    }


}