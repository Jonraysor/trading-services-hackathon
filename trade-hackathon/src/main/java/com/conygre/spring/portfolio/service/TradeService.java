package com.conygre.spring.portfolio.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;

import com.conygre.spring.portfolio.entities.Trade;

import org.bson.types.ObjectId;


public interface TradeService {
    

    Collection<Trade> getTrades();
    void addTrade(Trade trade);
    Trade findByTicker(String ticker);
    void updateState(ObjectId id, String state);
    Optional<Trade> findById(ObjectId id);
    void deleteById(ObjectId id);
    HashMap<String, Trade> getCurrentPositions();
    void sellStock(String ticker, double quantity);
    ArrayList<Trade> currentPosList();
    ArrayList<Trade> getEarnings(ArrayList<Trade> Portfolio);
    String getPortfolioProfit();
    String getBalance();
    ArrayList<Trade> getPctHoldings();
	
}